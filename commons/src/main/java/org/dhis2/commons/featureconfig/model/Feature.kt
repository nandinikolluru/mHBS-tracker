package org.dhis2.commons.featureconfig.model

enum class Feature(val description: String) {
    ANDROAPP_2557("Local Analytics 2.5"),
    ANDROAPP_2557_VG("Show visualization groups"),
    FORCE_DEFAULT_ANALYTICS("Force default TEI analytics"),
    ANDROAPP_2275("Display Event-Tei Relationships")
}
