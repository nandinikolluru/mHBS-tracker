// Generated by data binding compiler. Do not edit!
package dhis2.org.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import dhis2.org.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class ItemSingleValueBinding extends ViewDataBinding {
  @NonNull
  public final ImageView descriptionLabel;

  @NonNull
  public final Guideline guideline;

  @NonNull
  public final View indicatorLegend;

  @NonNull
  public final TextView singleValue;

  @NonNull
  public final TextView singleValueTitle;

  protected ItemSingleValueBinding(Object _bindingComponent, View _root, int _localFieldCount,
      ImageView descriptionLabel, Guideline guideline, View indicatorLegend, TextView singleValue,
      TextView singleValueTitle) {
    super(_bindingComponent, _root, _localFieldCount);
    this.descriptionLabel = descriptionLabel;
    this.guideline = guideline;
    this.indicatorLegend = indicatorLegend;
    this.singleValue = singleValue;
    this.singleValueTitle = singleValueTitle;
  }

  @NonNull
  public static ItemSingleValueBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.item_single_value, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static ItemSingleValueBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<ItemSingleValueBinding>inflateInternal(inflater, R.layout.item_single_value, root, attachToRoot, component);
  }

  @NonNull
  public static ItemSingleValueBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.item_single_value, null, false, component)
   */
  @NonNull
  @Deprecated
  public static ItemSingleValueBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<ItemSingleValueBinding>inflateInternal(inflater, R.layout.item_single_value, null, false, component);
  }

  public static ItemSingleValueBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static ItemSingleValueBinding bind(@NonNull View view, @Nullable Object component) {
    return (ItemSingleValueBinding)bind(component, view, R.layout.item_single_value);
  }
}
