package dhis2.org.analytics.charts.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\f\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0003"}, d2 = {"toChartBuilder", "Ldhis2/org/analytics/charts/data/Chart$ChartBuilder;", "Ldhis2/org/analytics/charts/data/Graph;", "dhis_android_analytics_dhisDebug"})
public final class GraphKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final dhis2.org.analytics.charts.data.Chart.ChartBuilder toChartBuilder(@org.jetbrains.annotations.NotNull()
    dhis2.org.analytics.charts.data.Graph $this$toChartBuilder) {
        return null;
    }
}