package dhis2.org.analytics.charts.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003\u00a2\u0006\u0002\u0010\rJ\t\u0010\u0011\u001a\u00020\u0005H\u00c6\u0003J.\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0005H\u00c6\u0001\u00a2\u0006\u0002\u0010\u0013J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0015\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\n\n\u0002\u0010\u000e\u001a\u0004\b\f\u0010\r\u00a8\u0006\u001b"}, d2 = {"Ldhis2/org/analytics/charts/data/GraphPoint;", "", "eventDate", "Ljava/util/Date;", "position", "", "fieldValue", "(Ljava/util/Date;Ljava/lang/Float;F)V", "getEventDate", "()Ljava/util/Date;", "getFieldValue", "()F", "getPosition", "()Ljava/lang/Float;", "Ljava/lang/Float;", "component1", "component2", "component3", "copy", "(Ljava/util/Date;Ljava/lang/Float;F)Ldhis2/org/analytics/charts/data/GraphPoint;", "equals", "", "other", "hashCode", "", "toString", "", "dhis_android_analytics_dhisDebug"})
public final class GraphPoint {
    @org.jetbrains.annotations.NotNull()
    private final java.util.Date eventDate = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Float position = null;
    private final float fieldValue = 0.0F;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Date getEventDate() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Float getPosition() {
        return null;
    }
    
    public final float getFieldValue() {
        return 0.0F;
    }
    
    public GraphPoint(@org.jetbrains.annotations.NotNull()
    java.util.Date eventDate, @org.jetbrains.annotations.Nullable()
    java.lang.Float position, float fieldValue) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Date component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Float component2() {
        return null;
    }
    
    public final float component3() {
        return 0.0F;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final dhis2.org.analytics.charts.data.GraphPoint copy(@org.jetbrains.annotations.NotNull()
    java.util.Date eventDate, @org.jetbrains.annotations.Nullable()
    java.lang.Float position, float fieldValue) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}