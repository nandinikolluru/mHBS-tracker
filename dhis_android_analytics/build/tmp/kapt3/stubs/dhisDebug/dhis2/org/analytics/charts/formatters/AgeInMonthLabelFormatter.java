package dhis2.org.analytics.charts.formatters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0016\u00a8\u0006\t"}, d2 = {"Ldhis2/org/analytics/charts/formatters/AgeInMonthLabelFormatter;", "Lcom/github/mikephil/charting/formatter/ValueFormatter;", "()V", "getAxisLabel", "", "value", "", "axis", "Lcom/github/mikephil/charting/components/AxisBase;", "dhis_android_analytics_dhisDebug"})
public final class AgeInMonthLabelFormatter extends com.github.mikephil.charting.formatter.ValueFormatter {
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String getAxisLabel(float value, @org.jetbrains.annotations.Nullable()
    com.github.mikephil.charting.components.AxisBase axis) {
        return null;
    }
    
    public AgeInMonthLabelFormatter() {
        super();
    }
}