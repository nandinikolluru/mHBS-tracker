package dhis2.org.analytics.charts.mappers;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\"\u0010\u0007\u001a\u0014\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\b2\u0006\u0010\u000b\u001a\u00020\fH\u0002J\"\u0010\r\u001a\u0014\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\b2\u0006\u0010\u000e\u001a\u00020\fH\u0002J\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0011\u001a\u00020\u0012H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"}, d2 = {"Ldhis2/org/analytics/charts/mappers/AnalyticTeiSettingsToSettingsAnalyticsModel;", "", "analyticDataElementMapper", "Ldhis2/org/analytics/charts/mappers/AnalyticDataElementToDataElementData;", "analyticIndicatorMapper", "Ldhis2/org/analytics/charts/mappers/AnalyticIndicatorToIndicatorData;", "(Ldhis2/org/analytics/charts/mappers/AnalyticDataElementToDataElementData;Ldhis2/org/analytics/charts/mappers/AnalyticIndicatorToIndicatorData;)V", "getAgeOrHeightContainer", "Lkotlin/Triple;", "", "", "x", "Lorg/hisp/dhis/android/core/settings/AnalyticsTeiWHONutritionItem;", "getZscoreContainer", "y", "map", "Ldhis2/org/analytics/charts/data/SettingsAnalyticModel;", "analyticsTeiSetting", "Lorg/hisp/dhis/android/core/settings/AnalyticsTeiSetting;", "mapDefault", "Ldhis2/org/analytics/charts/data/DefaultSettingsAnalyticModel;", "mapNutrition", "Ldhis2/org/analytics/charts/data/NutritionSettingsAnalyticsModel;", "dhis_android_analytics_dhisDebug"})
public final class AnalyticTeiSettingsToSettingsAnalyticsModel {
    private final dhis2.org.analytics.charts.mappers.AnalyticDataElementToDataElementData analyticDataElementMapper = null;
    private final dhis2.org.analytics.charts.mappers.AnalyticIndicatorToIndicatorData analyticIndicatorMapper = null;
    
    @org.jetbrains.annotations.NotNull()
    public final dhis2.org.analytics.charts.data.SettingsAnalyticModel map(@org.jetbrains.annotations.NotNull()
    org.hisp.dhis.android.core.settings.AnalyticsTeiSetting analyticsTeiSetting) {
        return null;
    }
    
    private final dhis2.org.analytics.charts.data.NutritionSettingsAnalyticsModel mapNutrition(org.hisp.dhis.android.core.settings.AnalyticsTeiSetting analyticsTeiSetting) {
        return null;
    }
    
    private final dhis2.org.analytics.charts.data.DefaultSettingsAnalyticModel mapDefault(org.hisp.dhis.android.core.settings.AnalyticsTeiSetting analyticsTeiSetting) {
        return null;
    }
    
    private final kotlin.Triple<java.lang.String, java.lang.String, java.lang.Boolean> getZscoreContainer(org.hisp.dhis.android.core.settings.AnalyticsTeiWHONutritionItem y) {
        return null;
    }
    
    private final kotlin.Triple<java.lang.String, java.lang.String, java.lang.Boolean> getAgeOrHeightContainer(org.hisp.dhis.android.core.settings.AnalyticsTeiWHONutritionItem x) {
        return null;
    }
    
    public AnalyticTeiSettingsToSettingsAnalyticsModel(@org.jetbrains.annotations.NotNull()
    dhis2.org.analytics.charts.mappers.AnalyticDataElementToDataElementData analyticDataElementMapper, @org.jetbrains.annotations.NotNull()
    dhis2.org.analytics.charts.mappers.AnalyticIndicatorToIndicatorData analyticIndicatorMapper) {
        super();
    }
}