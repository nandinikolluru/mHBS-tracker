package dhis2.org.analytics.charts.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\b\u0086\b\u0018\u00002\u00020\u0001BA\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\b\u0012\u0006\u0010\f\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\rJ\t\u0010\u0013\u001a\u00020\u0003H\u00c2\u0003J\t\u0010\u0014\u001a\u00020\u0003H\u00c2\u0003J\t\u0010\u0015\u001a\u00020\u0006H\u00c2\u0003J\u000f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u00c6\u0003J\u000f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u000b0\bH\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0003H\u00c6\u0003JQ\u0010\u0019\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\b2\b\b\u0002\u0010\f\u001a\u00020\u0003H\u00c6\u0001J\u000e\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0016J\u0013\u0010\u001b\u001a\u00020\u001c2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u00d6\u0003J\t\u0010\u001f\u001a\u00020 H\u00d6\u0001J\u000e\u0010!\u001a\b\u0012\u0004\u0012\u00020\u000b0\bH\u0016J\b\u0010\"\u001a\u00020\u0003H\u0016J\t\u0010#\u001a\u00020\u0003H\u00d6\u0001R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0017\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\f\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012\u00a8\u0006$"}, d2 = {"Ldhis2/org/analytics/charts/data/DefaultSettingsAnalyticModel;", "Ldhis2/org/analytics/charts/data/SettingsAnalyticModel;", "name", "", "program", "chartType", "Ldhis2/org/analytics/charts/data/ChartType;", "dataElementList", "", "Ldhis2/org/analytics/charts/data/DataElementData;", "indicatorList", "Ldhis2/org/analytics/charts/data/IndicatorData;", "stagePeriod", "(Ljava/lang/String;Ljava/lang/String;Ldhis2/org/analytics/charts/data/ChartType;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V", "getDataElementList", "()Ljava/util/List;", "getIndicatorList", "getStagePeriod", "()Ljava/lang/String;", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "dataElements", "equals", "", "other", "", "hashCode", "", "indicators", "period", "toString", "dhis_android_analytics_dhisDebug"})
public final class DefaultSettingsAnalyticModel extends dhis2.org.analytics.charts.data.SettingsAnalyticModel {
    private final java.lang.String name = null;
    private final java.lang.String program = null;
    private final dhis2.org.analytics.charts.data.ChartType chartType = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<dhis2.org.analytics.charts.data.DataElementData> dataElementList = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<dhis2.org.analytics.charts.data.IndicatorData> indicatorList = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String stagePeriod = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.util.List<dhis2.org.analytics.charts.data.DataElementData> dataElements() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.util.List<dhis2.org.analytics.charts.data.IndicatorData> indicators() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String period() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<dhis2.org.analytics.charts.data.DataElementData> getDataElementList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<dhis2.org.analytics.charts.data.IndicatorData> getIndicatorList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getStagePeriod() {
        return null;
    }
    
    public DefaultSettingsAnalyticModel(@org.jetbrains.annotations.NotNull()
    java.lang.String name, @org.jetbrains.annotations.NotNull()
    java.lang.String program, @org.jetbrains.annotations.NotNull()
    dhis2.org.analytics.charts.data.ChartType chartType, @org.jetbrains.annotations.NotNull()
    java.util.List<dhis2.org.analytics.charts.data.DataElementData> dataElementList, @org.jetbrains.annotations.NotNull()
    java.util.List<dhis2.org.analytics.charts.data.IndicatorData> indicatorList, @org.jetbrains.annotations.NotNull()
    java.lang.String stagePeriod) {
    }
    
    private final java.lang.String component1() {
        return null;
    }
    
    private final java.lang.String component2() {
        return null;
    }
    
    private final dhis2.org.analytics.charts.data.ChartType component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<dhis2.org.analytics.charts.data.DataElementData> component4() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<dhis2.org.analytics.charts.data.IndicatorData> component5() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component6() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final dhis2.org.analytics.charts.data.DefaultSettingsAnalyticModel copy(@org.jetbrains.annotations.NotNull()
    java.lang.String name, @org.jetbrains.annotations.NotNull()
    java.lang.String program, @org.jetbrains.annotations.NotNull()
    dhis2.org.analytics.charts.data.ChartType chartType, @org.jetbrains.annotations.NotNull()
    java.util.List<dhis2.org.analytics.charts.data.DataElementData> dataElementList, @org.jetbrains.annotations.NotNull()
    java.util.List<dhis2.org.analytics.charts.data.IndicatorData> indicatorList, @org.jetbrains.annotations.NotNull()
    java.lang.String stagePeriod) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}